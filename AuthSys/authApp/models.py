from django.db import models

class User(models.Model):
    id = models.AutoField(primary_key=True)
    nick = models.CharField(max_length=30)
    email = models.EmailField(max_length=30)
    password = models.CharField(max_length=30)
    repassword = models.CharField(max_length=30)
